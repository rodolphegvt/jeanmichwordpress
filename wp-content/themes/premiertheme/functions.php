<?php


/*Active image a la une */
add_theme_support( 'post-thumbnails' );
add_image_size( 'img_liste', 270, 220, array( 'center', 'top') );

add_action( 'init','create_post_type');
 function create_post_type(){

	register_post_type( 'projet',
		array(
			'labels' => array(
				'name' => __( 'Projet' ),
				'singular_name' => __( 'Projet' )
			),
			'public' => true,
			'supports' => array('title','editor','thumbnail' ),
			'has_archive' => true
		)
	);

	register_post_type( 'film',
		array(
			'labels' => array(
				'name' => __( 'films' ),
				'singular_name' => __( 'film' )
			),
			'public' => true,
			'supports' => array('title','editor','thumbnail','excerpt' ),
			'has_archive' => true
		)
	);

 register_taxonomy(
 	'type',
 	'projet',
 	array(
 		'label' => __( 'Type' ),
 		'hierarchical' => true,
 		)
 	);
 register_taxonomy(
 	'Sujet',
 	'film',
 	array(
 		'label' => __( 'Sujet' ),
 		'hierarchical' => true,
 		)
 	);

}
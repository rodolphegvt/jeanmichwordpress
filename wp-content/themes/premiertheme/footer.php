    <!-- Footer Area -->
    <section id="footer">
            <div class="small-2 columns left-footer">
                <ul>
                    <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook"></a></li>
                    <li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="twitter"></a></li>
                </ul>
            </div>
            <div class="medium-6 large-8 columns"></div>
            <div class="medium-4 large-2 columns right-footer">
                <ul>
                    <li><a href="#">About</a></li>
                    <li><a href="#">Mentions légales</a></li>
                </ul>
            </div>
    </section>
    <!-- End Footer --> 

    <?php wp_footer(); ?>
</body>
</html>

<?php get_header(); ?>
	<!-- Start COVER -->
	<div class="row cover">
		<div class="small-12 small-centered columns">
			<h1 class="small-12 columns text-center">VENEZ DECOUVRIR VOTRE RECETTE DU JOUR</h1>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/cover.png" alt="cover"></a>
		</div>
	</div>
	<!-- End Cover -->
	<!-- Start Articles -->
	<div class="row articles">
		<div class="small-4 columns">
			<h1 class="left">SORTIES</h1>
			<div class="small-12 columns label-couleur"></div>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/image_article.png" alt="cover"></a>
			<h2 class="sous_titre">L'EAU, UNE MANIERE DE VIE</h2>
			<h3 class="small-11 columns extrait">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
			<div class="tag right">#expo</div>
		</div>
		<div class="small-4 columns">
			<h1 class="left">FOOD</h1>
			<div class="small-12 columns label-couleur food"></div>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/meduse.png" alt="cover"></a>
			<h2 class="sous_titre">LA MEDUSE, UNE SALOPE DES MERS</h2>
			<h3 class="small-11 columns extrait">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
			<div class="tag right">#club</div>
		</div>
		<div class="small-4 columns">
			<h1 class="left">FUN</h1>
			<div class="small-12 columns label-couleur fun"></div>
			<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/img/fun.png" alt="cover"></a>
			<h2 class="sous_titre">LE BIRDIES, PLUS JAMAIS ?</h2>
			<h3 class="small-11 columns extrait">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt</h3>
			<div class="tag right">#bar</div>
		</div>
	</div>
	<!-- End articles -->
	<!-- Voir plus -->
	<div class="row">
		<div class="small-12 columns vplus">
			<a href="#"><h3 class="text-center">VOIR PLUS</h3></a>
		</div>
	</div>

<?php get_footer(); ?>
